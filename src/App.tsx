import React, { useMemo, useState } from "react";
import "./App.css";

import {
  Active,
  DndContext,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from "@dnd-kit/core";

import {
  SortableContext,
  arrayMove,
  sortableKeyboardCoordinates,
} from "@dnd-kit/sortable";
import { SortableOverlay } from "./components/SortableOverlay";
import {
  DragHandle,
  SortableItem,
} from "./components/SortableOverlay/SortableItem";

type ItemType = Array<{ id: string; title: string; order: number }>;

const dataArray: Array<{ id: string; title: string; order: number }> = [
  {
    id: "00",
    title: "a",
    order: 1,
  },
  {
    id: "01",
    title: "b",
    order: 2,
  },
  {
    id: "02",
    title: "c",
    order: 3,
  },
  {
    id: "03",
    title: "d",
    order: 4,
  },
  {
    id: "04",
    title: "e",
    order: 5,
  },
  {
    id: "05",
    title: "f",
    order: 6,
  },
];

const App = () => {
  const [items, setItems] = useState<ItemType>([...dataArray]);
  const [active, setActive] = useState<Active | null>(null);

  const activeItem = useMemo(
    () => items.find((item) => item.id === active?.id),
    [active, items]
  );

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );

  return (
    <DndContext
      sensors={sensors}
      onDragStart={({ active }) => {
        setActive(active);
      }}
      onDragEnd={({ active, over }) => {
        if (over && active.id !== over?.id) {
          const activeIndex = items.findIndex(({ id }) => id === active.id);
          const overIndex = items.findIndex(({ id }) => id === over.id);

          setItems(arrayMove(items, activeIndex, overIndex));
        }
        setActive(null);
      }}
      onDragCancel={() => {
        setActive(null);
      }}
    >
      <SortableContext items={items}>
        <ul className="SortableList" role="application">
          {items.map((item) => (
            <React.Fragment key={item.id}>
              <SortableItem id={item.id}>
                {item.id}
                <DragHandle />
              </SortableItem>
            </React.Fragment>
          ))}
        </ul>
      </SortableContext>
      <SortableOverlay>
        {activeItem ? (
          <React.Fragment key={activeItem.id}>
            <SortableItem id={activeItem.id}>
              {activeItem.id}
              <DragHandle />
            </SortableItem>
          </React.Fragment>
        ) : null}
      </SortableOverlay>
    </DndContext>
  );
};

export default App;
